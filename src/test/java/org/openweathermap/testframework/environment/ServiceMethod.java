package org.openweathermap.testframework.environment;

public enum  ServiceMethod {


    //CURRENT WEATHER

    ONE_LOCATION("weather"),
    SEVERAL_RECTANGLE("box/city"),
    SEVERAL_CYCLE("find"),
    SEVERAL_IDS("group"),

    //5 DAY - 3 HOUR

    FORECAST("forecast");

    private String method;

    ServiceMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return this.method;
    }

    @Override
    public String toString() {
        return this.method;
    }

}
