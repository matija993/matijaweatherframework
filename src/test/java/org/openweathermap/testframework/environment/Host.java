package org.openweathermap.testframework.environment;

public enum Host {

    PROD("http://api.openweathermap.org/data/2.5");

    private String url;

    public String getUrl() {
        return this.url;
    }

    private Host(String url) {
        this.url = url;
    }

    public String createUrl(ServiceMethod method) {
        StringBuilder url = new StringBuilder(this.url);
        url.append("/").append(method.toString());
        return url.toString();
    }

}
