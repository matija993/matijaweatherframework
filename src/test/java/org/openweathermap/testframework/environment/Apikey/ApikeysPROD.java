package org.openweathermap.testframework.environment.Apikey;

public class ApikeysPROD extends Apikeys{

    private static ApikeyObject DEFAULT = new ApikeyObject();
    private static ApikeyObject EXTRA = new ApikeyObject();
    private static ApikeyObject BONUS = new ApikeyObject();

    public static Apikeys getApikeys() {
        return new ApikeysPROD();
    }

    static {

        DEFAULT.setKey("c7fb53084f4e1c479f623772aac5fd44");
        EXTRA.setKey("23190f0f9facccbc1a6dd0fbd85a8a4b");
        BONUS.setKey("88dab9b43c53e10d05b5034750dac1eb");
    }

    @Override
    public ApikeyObject getDefault() {
        return DEFAULT;
    }

    @Override
    public ApikeyObject getExtra() {
        return EXTRA;
    }

    @Override
    public ApikeyObject getBonus() {
        return BONUS;
    }
}
