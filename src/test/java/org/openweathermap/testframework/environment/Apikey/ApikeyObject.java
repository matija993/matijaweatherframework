package org.openweathermap.testframework.environment.Apikey;

import lombok.Data;

@Data
public class ApikeyObject {

    private String key;
}
