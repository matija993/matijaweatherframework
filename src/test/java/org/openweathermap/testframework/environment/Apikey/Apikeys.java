package org.openweathermap.testframework.environment.Apikey;

public abstract class Apikeys {

    public abstract ApikeyObject getDefault();
    public abstract ApikeyObject getExtra();
    public abstract ApikeyObject getBonus();
}
