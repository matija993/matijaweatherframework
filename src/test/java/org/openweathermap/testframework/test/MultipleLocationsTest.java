package org.openweathermap.testframework.test;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openweathermap.testframework.environment.Apikey.ApikeyObject;
import org.openweathermap.testframework.environment.Apikey.ApikeysPROD;
import org.openweathermap.testframework.environment.Host;
import org.openweathermap.testframework.environment.Init;
import org.openweathermap.testframework.environment.ServiceMethod;
import org.openweathermap.testframework.models.current.CWCitiesInCycle;
import org.openweathermap.testframework.models.current.CWCitiesInRectangleZone;
import org.openweathermap.testframework.models.current.CWMultipleCityIds;
import org.openweathermap.testframework.models.current.CWSingleLocation;
import org.openweathermap.testframework.models.error.ErrorResponse;
import org.openweathermap.testframework.resources.convert.Convert;
import org.openweathermap.testframework.resources.http.HTTPClient;
import org.openweathermap.testframework.resources.http.HTTPGet;
import org.openweathermap.testframework.resources.http.HTTPResponse;

import java.util.Arrays;
import java.util.List;

@DisplayName("Current Weather - Multiple Locations")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Tag("currentweather")
class MultipleLocationsTest {

    private static final ServiceMethod methodRectangle = ServiceMethod.SEVERAL_RECTANGLE;
    private static final ServiceMethod methodCycle = ServiceMethod.SEVERAL_CYCLE;
    private static final ServiceMethod methodSeveralIds = ServiceMethod.SEVERAL_IDS;
    private static final Host host = Init.env;
    private static final String urlRectangle = host.createUrl(methodRectangle);
    private static final String urlCycle = host.createUrl(methodCycle);
    private static final String urlSeveralIds = host.createUrl(methodSeveralIds);
    private final ApikeyObject defaultApikey = ApikeysPROD.getApikeys().getDefault();
    private final ApikeyObject extraApikey = ApikeysPROD.getApikeys().getExtra();
    private final ApikeyObject bonusApikey = ApikeysPROD.getApikeys().getBonus();
    private static final String APIKEY = "APPID";
    private static final String ID = "id";
    private static final String LATITUDE = "lat";
    private static final String LONGITUDE = "lon";
    private static final String RECTANGLE = "bbox";



    @DisplayName("Search within rectangle zone")
    @Test
    void rectangle_zone() throws Exception {
        HTTPClient client = HTTPGet.getClient(urlRectangle);
        client.addQueryParam(RECTANGLE, 12,32,15,37,10);
        client.addQueryParam(APIKEY, defaultApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 200, response.getCode());
        CWCitiesInRectangleZone weatherResults = Convert.fromJson(response.getBody(), CWCitiesInRectangleZone.class);
        Check.forTrue("Bad result. ", weatherResults.getCnt() > 1);
    }

    @DisplayName("Cities in cycle - invalid search")
    @ParameterizedTest(name = "{2}")
    @CsvSource({"abc, 20.47, latitude", "44.8, abc, longitude"})
    void cycle_invalid(String latitude, String longitude, String test_name) throws Exception {
        HTTPClient client = HTTPGet.getClient(urlCycle);
        client.addQueryParam(LATITUDE, latitude);
        client.addQueryParam(LONGITUDE, longitude);
        client.addQueryParam(APIKEY, extraApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 400, response.getCode());
        ErrorResponse error = Convert.fromJson(response.getBody(), ErrorResponse.class);
        Check.forTrue("Bad error code: " + error.getCod(), error.getCod().equals(response.getCodeString()));
        Check.forTrue("Bad error message: " + error.getMessage(), error.getMessage().equals("abc is not a float"));
    }

    @DisplayName("Cities in cycle search")
    @Test
    void cycle_search() throws Exception {
        HTTPClient client = HTTPGet.getClient(urlCycle);
        client.addQueryParam(LATITUDE, 55.5);
        client.addQueryParam(LONGITUDE, 37.5);
        client.addQueryParam(APIKEY, bonusApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 200, response.getCode());
        CWCitiesInCycle weatherResult = Convert.fromJson(response.getBody(), CWCitiesInCycle.class);
        CWSingleLocation city = weatherResult.getList().get(1);
        Check.forTrue("Bad latitude. ", city.getCoord().getLat() > 55.0);
        Check.forTrue("Bad longitude. ", city.getCoord().getLon() > 37.0);
    }

    @DisplayName("Multiple IDs - invalid")
    @Test
    void multiple_invalid() throws Exception {
        HTTPClient client = HTTPGet.getClient(urlSeveralIds);
        String invalidIds = "abc";
        client.addQueryParam(ID, invalidIds);
        client.addQueryParam(APIKEY, defaultApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 400, response.getCode());
        ErrorResponse error = Convert.fromJson(response.getBody(), ErrorResponse.class);
        Check.forTrue("Bad error code: " + error.getCod(), error.getCod().equals(response.getCodeString()));
        Check.forTrue("Bad error message: " + error.getMessage(), error.getMessage().equals(invalidIds + " is not a city id"));
    }

    @DisplayName("Search multiple city IDs")
    @Test
    void multiple_search() throws Exception {
        HTTPClient client = HTTPGet.getClient(urlSeveralIds);
        List<String> cityIDs = Arrays.asList("524901", "703448", "2643743");
        client.addQueryParam(ID, cityIDs);
        client.addQueryParam(APIKEY, defaultApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 200, response.getCode());
        CWMultipleCityIds weatherResult = Convert.fromJson(response.getBody(), CWMultipleCityIds.class);
        Check.forEquals("Bad number of cities. ", 3, weatherResult.getCnt());
        Check.forTrue("Bad city IDs. ", weatherResult.hasOnlyCityIDs(cityIDs));
    }
}
