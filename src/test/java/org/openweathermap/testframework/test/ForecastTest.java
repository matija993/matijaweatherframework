package org.openweathermap.testframework.test;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openweathermap.testframework.environment.Apikey.ApikeyObject;
import org.openweathermap.testframework.environment.Apikey.ApikeysPROD;
import org.openweathermap.testframework.environment.Host;
import org.openweathermap.testframework.environment.Init;
import org.openweathermap.testframework.environment.ServiceMethod;
import org.openweathermap.testframework.models.current.CWSingleLocation;
import org.openweathermap.testframework.models.error.ErrorResponse;
import org.openweathermap.testframework.models.longterm.Forecast;
import org.openweathermap.testframework.resources.convert.Convert;
import org.openweathermap.testframework.resources.general.Utils;
import org.openweathermap.testframework.resources.http.HTTPClient;
import org.openweathermap.testframework.resources.http.HTTPGet;
import org.openweathermap.testframework.resources.http.HTTPResponse;

@DisplayName("5 day / 3 hour forecast")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Tag("forecast")
class ForecastTest {

    private static final ServiceMethod method = ServiceMethod.FORECAST;
    private static final Host host = Init.env;
    private static final String url = host.createUrl(method);
    private final ApikeyObject defaultApikey = ApikeysPROD.getApikeys().getDefault();
    private final ApikeyObject extraApikey = ApikeysPROD.getApikeys().getExtra();
    private final ApikeyObject bonusApikey = ApikeysPROD.getApikeys().getBonus();
    private static final String APIKEY = "APPID";
    private static final String QUERY = "q";
    private static final String ID = "id";
    private static final String LATITUDE = "lat";
    private static final String LONGITUDE = "lon";
    private static final String ZIP_CODE = "zip";



    @DisplayName("Missing query input")
    @Test
    void missing_query() throws Exception {
        HTTPClient client = HTTPGet.getClient(url);
        client.addQueryParam(QUERY, "");
        client.addQueryParam(APIKEY, defaultApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 400, response.getCode());
        ErrorResponse error = Convert.fromJson(response.getBody(), ErrorResponse.class);
        Check.forTrue("Bad error code: " + error.getCod(), error.getCod().equals(response.getCodeString()));
        Check.forTrue("Bad error message: " + error.getMessage(), error.getMessage().equals("Nothing to geocode"));
    }

    @DisplayName("Invalid query")
    @Test
    void invalid_query() throws Exception {
        String invalid = Utils.generateString(10);
        HTTPClient client = HTTPGet.getClient(url);
        client.addQueryParam(QUERY, invalid);
        client.addQueryParam(APIKEY, extraApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 404, response.getCode());
        ErrorResponse error = Convert.fromJson(response.getBody(), ErrorResponse.class);
        Check.forTrue("Bad error code: " + error.getCod(), error.getCod().equals(response.getCodeString()));
        Check.forTrue("Bad error message: " + error.getMessage(), error.getMessage().equals("city not found"));
    }

    @DisplayName("Search by city name")
    @Test
    void city_name() throws Exception {
        String city = "Belgrade";
        HTTPClient client = HTTPGet.getClient(url);
        client.addQueryParam(QUERY, city);
        client.addQueryParam(APIKEY, bonusApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 200, response.getCode());
        Forecast forecast = Convert.fromJson(response.getBody(), Forecast.class);
        Check.forEquals("Bad city name. ", city, forecast.getCity().getName());
    }

    @DisplayName("Search by city name and country code")
    @Test
    void city_name_code() throws Exception {
        String city = "Belgrade";
        String country = "RS";
        HTTPClient client = HTTPGet.getClient(url);
        client.addQueryParam(QUERY, city,country);
        client.addQueryParam(APIKEY, defaultApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 200, response.getCode());
        Forecast forecast = Convert.fromJson(response.getBody(), Forecast.class);
        Check.forEquals("Bad city name. ", city, forecast.getCity().getName());
        Check.forEquals("Bad country code. ", country, forecast.getCity().getCountry());
    }

    @DisplayName("Invalid city id")
    @Test
    void invalid_id() throws Exception {
        String invalid_id = Utils.generateString(5);
        HTTPClient client = HTTPGet.getClient(url);
        client.addQueryParam(ID, invalid_id);
        client.addQueryParam(APIKEY, extraApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 400, response.getCode());
        ErrorResponse error = Convert.fromJson(response.getBody(), ErrorResponse.class);
        Check.forTrue("Bad error code: " + error.getCod(), error.getCod().equals(response.getCodeString()));
        Check.forTrue("Bad error message: " + error.getMessage(), error.getMessage().equals(invalid_id + " is not a city ID"));
    }

    @DisplayName("Search by city ID")
    @Test
    void city_id() throws Exception {
        Integer city_id = 792680;
        HTTPClient client = HTTPGet.getClient(url);
        client.addQueryParam(ID, city_id);
        client.addQueryParam(APIKEY, bonusApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 200, response.getCode());
        Forecast forecast = Convert.fromJson(response.getBody(), Forecast.class);
        Check.forEquals("Bad city ID. ", city_id, forecast.getCity().getId());
    }

    @DisplayName("Invalid geo location")
    @ParameterizedTest(name = "{2}")
    @CsvSource({"abc, 20.47, latitude", "44.8, abc, longitude"})
    void invalid_geo(String latitude, String longitude, String test_name) throws Exception {
        HTTPClient client = HTTPGet.getClient(url);
        client.addQueryParam(LATITUDE, latitude);
        client.addQueryParam(LONGITUDE, longitude);
        client.addQueryParam(APIKEY, defaultApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 400, response.getCode());
        ErrorResponse error = Convert.fromJson(response.getBody(), ErrorResponse.class);
        Check.forTrue("Bad error code: " + error.getCod(), error.getCod().equals(response.getCodeString()));
        Check.forTrue("Bad error message: " + error.getMessage(), error.getMessage().equals("abc is not a float"));
    }

    @DisplayName("Search by geo location")
    @Test
    void lat_lon() throws Exception {
        String latitude = "44.8";
        String longitude = "20.47";
        HTTPClient client = HTTPGet.getClient(url);
        client.addQueryParam(LATITUDE, latitude);
        client.addQueryParam(LONGITUDE, longitude);
        client.addQueryParam(APIKEY, extraApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 200, response.getCode());
        Forecast forecast = Convert.fromJson(response.getBody(), Forecast.class);
        Check.forTrue("Bad latitude. ", forecast.getCity().getCoord().getLat() > 44);
        Check.forTrue("Bad longitude. ", forecast.getCity().getCoord().getLon() > 20);
    }

    @DisplayName("Search by zip code")
    @Test
    void invalid_zipCode() throws Exception {
        HTTPClient client = HTTPGet.getClient(url);
        client.addQueryParam(ZIP_CODE, 99501);
        client.addQueryParam(APIKEY, bonusApikey.getKey());
        HTTPResponse response = client.getResponse();
        Check.forEquals("Bad response code: ", 200, response.getCode());
        Forecast forecast = Convert.fromJson(response.getBody(), Forecast.class);
        Check.forEquals("Bad city returned. ", "Anchorage", forecast.getCity().getName());
    }

}
