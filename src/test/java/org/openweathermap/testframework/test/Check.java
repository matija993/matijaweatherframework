package org.openweathermap.testframework.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Check {

    public static void forEquals(String message, Object expected, Object actual) {
        assertEquals(expected, actual, message);
    }

    public static void forTrue(String message, boolean condition) {
        assertTrue(condition, message);
    }

    public static void forFalse(String message, boolean condition) {
        assertFalse(condition, message);
    }

    public static void forNotNull(String message, Object o) {
        assertNotNull(o, message);
    }
}
