package org.openweathermap.testframework.resources.general;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Random;

public class Utils {

    public static String generateString(int length) throws Exception {
        if(length < 0) throw new IllegalArgumentException("Random String length must be > 0.");
        if(length == 0) return StringUtils.EMPTY;
        String sourceString = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
        StringBuilder sb = new StringBuilder();
        for(int i = 1; i <= length; i++) {
            sb.append((sourceString.charAt(new Random().nextInt(sourceString.length()-1))));
        }
        return sb.toString();
    }

    public static <T> boolean sameContent(Collection<T> listOne, Collection<T> listTwo) {
        if(listOne == null || listTwo == null) return false;
        if(listOne.size() != listTwo.size()) return false;
        return (listOne.containsAll(listTwo) && listTwo.containsAll(listOne));
    }
}
