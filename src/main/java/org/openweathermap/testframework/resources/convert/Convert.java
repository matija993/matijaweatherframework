package org.openweathermap.testframework.resources.convert;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Convert {

    public static <T> T fromJson(String json, Class<T> destination) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        return mapper.readValue(json, destination);
    }

    @SuppressWarnings("unchecked")
    public static <T> T fromXml(String xml, Class<T> destination) throws Exception {
        JAXBContext context = JAXBContext.newInstance(destination);
        Unmarshaller unmar = context.createUnmarshaller();
        return (T) unmar.unmarshal(new StringReader(xml));
    }

    public static <T> T from(String body, String format, Class<T> destination) throws Exception {
        if(format.equalsIgnoreCase("json")) return fromJson(body, destination);
        else if(format.equalsIgnoreCase("xml")) return fromXml(body, destination);
        else throw new IllegalArgumentException("Format not allowed");
    }

}