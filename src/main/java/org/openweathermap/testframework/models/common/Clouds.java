package org.openweathermap.testframework.models.common;

import lombok.Data;

@Data
public class Clouds {

    private Integer all;
    private Integer today;
}
