package org.openweathermap.testframework.models.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Snow {

    @JsonProperty("1h")
    private Integer oneHour;
    @JsonProperty("3h")
    private Integer threeHours;
}
