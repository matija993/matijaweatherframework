package org.openweathermap.testframework.models.common;

import lombok.Data;

@Data
public class Wind {

    private Float speed;
    private Integer deg;
    private Integer gust;
}
