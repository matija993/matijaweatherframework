package org.openweathermap.testframework.models.common;

import lombok.Data;

@Data
public class Coord {

    private Float lon;
    private Float lat;

}
