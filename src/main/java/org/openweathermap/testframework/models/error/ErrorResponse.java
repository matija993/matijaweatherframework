package org.openweathermap.testframework.models.error;

import lombok.Data;

@Data
public class ErrorResponse {
    private String cod;
    private String message;
}
