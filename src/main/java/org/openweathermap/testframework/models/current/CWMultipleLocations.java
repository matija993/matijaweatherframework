package org.openweathermap.testframework.models.current;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CWMultipleLocations {

    protected List<CWSingleLocation> list;

    public List<String> getCityIDs() {
        List<String> found = new ArrayList<>();
        for (CWSingleLocation city : this.list) found.add(city.getId().toString());
        return found;
    }

    public CWSingleLocation getCityWeatherByID(String id) {
        for (CWSingleLocation cityWeather : this.list)
            if (cityWeather.getId().equals(id)) return cityWeather;
        return null;
    }
}
