package org.openweathermap.testframework.models.current;

import lombok.Data;
import org.openweathermap.testframework.models.common.*;

import java.util.List;

@Data
public class CWSingleLocation {

    private Coord coord;
    private List<Weather> weather;
    private String base;
    private Main main;
    private Integer visibility;
    private Wind wind;
    private Clouds clouds;
    private Rain rain;
    private Snow snow;
    private Long dt;
    private Sys sys;
    private Integer timezone;
    private Integer id;
    private String name;
    private Integer cod;

}
