package org.openweathermap.testframework.models.current;

import lombok.Data;

@Data
public class Sys {

    private Integer type;
    private Integer id;
    private Integer timezone;
    private Float message;
    private String country;
    private Long sunrise;
    private Long sunset;
}
