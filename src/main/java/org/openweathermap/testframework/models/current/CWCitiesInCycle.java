package org.openweathermap.testframework.models.current;

import lombok.Data;

@Data
public class CWCitiesInCycle extends CWMultipleLocations{

    private String message;
    private String cod;
    private Integer count;
}
