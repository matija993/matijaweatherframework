package org.openweathermap.testframework.models.current;

import lombok.Data;

@Data
public class CWCitiesInRectangleZone extends CWMultipleLocations{

    private String cod;
    private Float calctime;
    private Integer cnt;
}
