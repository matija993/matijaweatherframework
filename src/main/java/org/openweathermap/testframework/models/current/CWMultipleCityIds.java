package org.openweathermap.testframework.models.current;

import lombok.Data;
import org.openweathermap.testframework.resources.general.Utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

@Data
public class CWMultipleCityIds extends CWMultipleLocations{

    private Integer cnt;

    public boolean hasOnlyCityIDs (Collection<String> ids) {
        if (this.list.isEmpty()) return false;
        return Utils.sameContent(ids, new HashSet<>(this.getCityIDs()));
    }

    public boolean hasOnlyCityIDs(String...ids) {
        return this.hasOnlyCityIDs(Arrays.asList(ids));
    }
}
