package org.openweathermap.testframework.models.longterm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Forecast {

    @JsonProperty("cod")
    private String code;
    private String message;
    private City city;
    private Integer cnt;
    private List<org.openweathermap.testframework.models.longterm.List> list;

}
