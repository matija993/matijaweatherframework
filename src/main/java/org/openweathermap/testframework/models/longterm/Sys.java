package org.openweathermap.testframework.models.longterm;

import lombok.Data;

@Data
public class Sys {

    private String pod;
}
