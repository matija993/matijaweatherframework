package org.openweathermap.testframework.models.longterm;

import lombok.Data;
import org.openweathermap.testframework.models.common.Coord;

@Data
public class City {

    private Integer id;
    private String name;
    private Coord coord;
    private String country;
    private Integer population;
    private String timezone;
    private String sunrise;
    private String sunset;
}
