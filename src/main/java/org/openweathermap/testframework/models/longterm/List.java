package org.openweathermap.testframework.models.longterm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.openweathermap.testframework.models.common.*;

@Data
public class List {

    @JsonProperty("dt")
    private Long dateTime;
    private Main main;
    private java.util.List<Weather> weather;
    private Clouds clouds;
    private Wind wind;
    private Rain rain;
    private Snow snow;
    private Sys sys;
    @JsonProperty("dt_txt")
    private String dateTimeText;
}
