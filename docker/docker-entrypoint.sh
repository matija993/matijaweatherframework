#!/bin/bash

sed -i "s|ADMIN_ADDRESSES_PLACEHOLDER|${JENKINS_LOCAL_CONFIG_ADMIN_ADDRESSES}|" /var/jenkins_home/jenkins.model.JenkinsLocationConfiguration.xml
sed -i "s|JENKINS_URL_PLACEHOLDER|${JENKINS_LOCAL_CONFIG_JENKINS_URL}|" /var/jenkins_home/jenkins.model.JenkinsLocationConfiguration.xml
sed -i "s|<quietPeriod>5</quietPeriod>|<quietPeriod>0</quietPeriod>|" /var/jenkins_home/config.xml

/sbin/tini -- /usr/local/bin/jenkins.sh